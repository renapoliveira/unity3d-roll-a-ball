﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour {

    public float speed;
    public Text countText;
    public Text winText;

    private Rigidbody rb;
    private int count;
    public float g = 9.8f;

    void Start()
    {
        //Next line is for PC
        rb = GetComponent<Rigidbody>();
        count = 0;
        setCountText();
        winText.text = "";
    }

	void FixedUpdate ()
    {
       
        if (Application.platform == RuntimePlatform.Android)
        {
            // normalize axis
            var gravity = new Vector3(
                Input.acceleration.x,
                Input.acceleration.z,
                Input.acceleration.y
            ) * g;

            GetComponent<Rigidbody>().AddForce(gravity, ForceMode.Acceleration);
            
        } else {
            //PC
            float moveHorizontal = Input.GetAxis("Horizontal");
            float moveVertical = Input.GetAxis("Vertical");
            Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
            rb.AddForce(movement * speed);
        }


    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            setCountText();
        }
    }

    void setCountText()
    {
        countText.text = "Contador: " + count.ToString();
        if (count >= 7)
        {
            winText.text = "Parabéns!";
        }
    }
}
